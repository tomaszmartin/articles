from html.parser import HTMLParser
from typing import List, Optional, Tuple


class Parser(HTMLParser):
    """
    Parses html and creates markdown of of it.
    For the following html code:
        <div><span class="klass">This</span> is jus a test</div>
    The result should be as follows:
        
    """

    def handle_starttag(self, tag: str, attrs: List[Tuple[str, Optional[str]]]) -> None:
        """
        Handle opening tag.
        """
        pass

    def handle_data(self, data: str) -> None:
        """
        Handle tag data.
        """
        pass

    def handle_endtag(self, tag: str) -> None:
        """
        Handle closing tag.
        """
        pass
