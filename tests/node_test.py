from app.markdown import Node


def test_node_comparison():
    n1 = Node("html", {})
    n2 = Node("html", {})
    assert n1._compare_node(n2)


def test_node_equality():
    n1 = Node("html", {})
    n2 = Node("html", {})
    assert n1 == n2


def test_node_equality_attrs():
    n1 = Node("html", {"test": "test"})
    n2 = Node("html", {})
    assert not n1 == n2


def test_node_equality_child():
    n1 = Node("html", {})
    n2 = Node("html", {})
    n3 = Node("div", {})
    n1.add_child(n3)
    assert not n1 == n2


def test_node_equality_parent():
    n1 = Node("html", {})
    n2 = Node("div", {})
    n3 = Node("div", {})
    n1.add_child(n3)
    assert not n3 == n2
