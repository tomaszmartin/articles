"""
Creating markdown text from raw html.
"""
from copy import deepcopy
from html.parser import HTMLParser
import re
from typing import Dict, List, Optional, Tuple

# TODO
# - newlines should be ignored in most tags


class Node:
    """Represents an html node"""

    def __init__(self, name: str, attrs: Dict[str, Optional[str]]) -> None:
        self.name = name.lower()
        self.attrs = attrs
        self.content = ""
        self.parent: Optional[Node] = None
        self.children: List[Node] = []
        self.level = 0

    def __repr__(self) -> str:
        attrs = {"attrs": self.attrs, "level": self.level, "content": self.content}
        return f"{self.name} ({attrs})"

    def add_child(self, child) -> None:
        """
        Adds a child to the node.
        """
        self.children.append(child)
        child.level += self.level + 1
        child.parent = self

    def build_tree(self, node=None) -> str:
        """
        Builds document tree for easy inspecting.
        """
        if not node:
            node = self
        indentation = "\t" * node.level
        text = f"\n{indentation}{node}"
        for child in node.children:
            text += self.build_tree(child)
        return text

    def _compare_node(self, another) -> bool:
        if not another:
            return False
        return (
            (self.name == another.name)
            and (self.attrs == another.attrs)
            and (self.content == another.content)
            and (self.level == another.level)
        )

    def __eq__(self, another):
        if not another:
            return False
        if not self._compare_node(another):
            return False
        # If only one parent is None
        if (self.parent != None) != (another.parent != None):
            return False
        # If both are not None
        if self.parent and another.parent:
            self.parent._compare_node(another.parent)
        # If they differ in length than elements doesn't matter
        if len(self.children) != len(another.children):
            return False
        for first, second in zip(self.children, another.children):
            if not first._compare_node(second):
                return False
        return True


class Converter:
    """
    Responsible for converting html into markdown.
    """

    ELEMENTS = {
        "a": "[{data}]({href})",
        "abbr": "{data}",
        "address": "",
        "article": "{data}",
        "aside": "",  # Not sure how to handle this
        "audio": "",
        "b": "**{data}**",
        "bdi": "{data}",  # Not sure how to handle this
        "bdo": "{data}",  # Not sure how to handle this
        "blockquote": "\n> {data}\n\n",
        "body": "{data}",
        "br": "\n{data}",
        "button": "",  # Not sure how to handle this
        "canvas": "",
        "caption": "{data}",  # Not sure how to handle this
        "cite": "*{data}*",
        "code": "`{data}`",
        "col": "{data}",  # Not sure how to handle this
        "colgroup": "{data}",  # Not sure how to handle this
        "data": "{data}",
        "dd": "{data}",  # Not sure how to handle this
        "del": "~~{data}~~",
        "details": "",  # Not sure how to handle this
        "dfn": "{data}",
        "dialog": "",
        "div": "{data}\n",
        "dl": "{data}",  # Not sure how to handle this
        "dt": "{data}",
        "em": "*{data}*",
        "embed": "",
        "fieldset": "",
        "figcaption": "",
        "figure": "",
        "font": "{data}",
        "footer": "",
        "form": "",
        "frame": "",
        "frameset": "",
        "h1": "# {data}\n",
        "h2": "## {data}\n",
        "h3": "### {data}\n",
        "h4": "#### {data}\n",
        "h5": "##### {data}\n",
        "h6": "###### {data}\n",
        "head": "{data}",
        "header": "{data}",
        "hr": "\n---\n\n{data}",
        "html": "{data}",
        "i": "*{data}*",
        "iframe": "",
        "img": "![]({src})\n\n",
        "ins": "{data}",
        "kbd": "",
        "label": "",
        "legend": "",
        "li": "- {data}",
        "link": "",
        "main": "{data}",
        "map": "",
        "mark": "{data}",
        "meta": "",
        "meter": "",
        "nav": "",
        "noscript": "",
        "object": "",
        "ol": "{data}",
        "optgroup": "",
        "option": "",
        "output": "",
        "p": "{data}\n",
        "param": "",
        "picture": "",
        "pre": "\n```\n{data}\n```\n\n",
        "progress": "",
        "q": "\n> {data}\n\n",
        "rp": "",
        "rt": "",
        "ruby": "",
        "s": "~~{data}~~",
        "samp": "\n```\n{data}\n```\n\n",
        "script": "",
        "section": "{data}",
        "select": "",
        "small": "{data}",
        "source": "",
        "span": "{data}",
        "strong": "**{data}**",
        "style": "",
        "sub": "{data}",
        "summary": "",
        "sup": "{data}",
        "svg": "",
        "table": "\n{data}\n\n",
        "tbody": "{data}",
        "td": "|{data}",
        "template": "",
        "textarea": "",
        "tfoot": "",
        "th": "|{data}",
        "thead": "{data}",
        "time": "{data}",
        "title": "{data}",
        "tr": "\n{data}",
        "track": "",
        "u": "{data}",
        "ul": "{data}",
        "var": "`{data}`",
        "video": "",
        "wbr": "{data}",
    }

    def convert(self, node: Node) -> str:
        """
        Returns html node as markdown.
        """
        for child in node.children:
            child_content = self.convert(child)
            node.content += child_content
        result = self._convert_single_node(node)
        result = self._clear_result(result)
        return result

    @staticmethod
    def _clear_result(formatted: str) -> str:
        formatted = re.sub(r"```\n\n+", "```\n", formatted)
        formatted = re.sub(r"\n\n```", "\n```", formatted)
        return formatted

    def _convert_single_node(self, node: Node) -> str:
        """
        Converts single html node into markdown text.
        """
        markdown = self.ELEMENTS.get(node.name, "{data}")
        if node.name == "a":
            node.content = re.sub(r"\n", "", node.content)
        # Column separators after head
        if node.name == "tr":
            cols = [col for col in node.children if col.name == "th"]
            if cols:
                markdown = "{data}\n" + (len(cols) * "|:-")
        # Inline code inside multiline code
        elif node.name == "code":
            if node.parent and node.parent.name == "pre":
                markdown = "{data}"
        # Numbering ordered lists
        elif node.name == "li":
            if node.parent and node.parent.name == "ol":
                points = [n for n in node.parent.children if n.name == "li"]
                position = points.index(node) + 1
                markdown = f"{position}. " + "{data}"
        attrs = deepcopy(node.attrs)
        # In case node has some data but not all of it
        args: Dict[str, Optional[str]] = {"data": node.content, "href": "", "src": ""}
        args.update(attrs)
        formatted = f"{markdown.format(**args)}"
        if self._contains_information(node, formatted):
            if node.name != "pre":
                # Strip new line with space to just newline
                formatted = re.sub(r"\n ", "\n", formatted)
                # Replace more than two newlines with single one
                formatted = re.sub(r"\n\n+", "\n\n", formatted)
            return formatted
        return ""

    def _contains_information(self, node: Node, formatted: str) -> bool:
        """
        Checks formatted representation contains any information.
        Builds a representation for empty node and verifies if it
        is the same as current formatted one.
        """
        if node.name == "br":
            return True
        markdown = self.ELEMENTS.get(node.name, "{data}")
        args = {"data": "", "href": "", "src": ""}
        empty = f"{markdown.format(**args)}"
        return empty != formatted


class Parser(HTMLParser):
    """Parses html and creates markdown of of it"""

    def __init__(self, *args, **kwargs):
        HTMLParser.__init__(self, *args, **kwargs)
        self.root = Node("html", {})
        self.open_tags = [self.root]

    def handle_starttag(self, tag: str, attrs: List[Tuple[str, Optional[str]]]) -> None:
        """
        Handle opening tag.
        """
        current_node = Node(tag, dict(attrs))
        self.open_tags[-1].add_child(current_node)
        self.open_tags.append(current_node)

    def handle_data(self, data: str) -> None:
        """
        Handle tag data.
        """
        current_node = Node("data", {})
        current_node.content = data
        self.open_tags[-1].add_child(current_node)

    def handle_endtag(self, tag: str) -> None:
        """
        Handle closing tag.
        """
        if self.open_tags[-1].name == tag:
            self.open_tags.pop()

    @staticmethod
    def _remove_whitespace(data: str) -> str:
        """
        Removes multiple whitespace from data.
        """
        spaces = re.compile(r" +", re.M)
        newlines = re.compile(r"\n+", re.M)

        stripped = re.sub(spaces, " ", data)
        stripped = re.sub(newlines, "", stripped)
        return stripped

    def parse(self, html: str) -> Node:
        self.feed(html)
        parsed = self.root
        self.root = Node("html", {})
        return parsed


def parse(html: str) -> Node:
    return Parser().parse(html)


def from_html(html: str) -> str:
    """
    Converts html text into markdown.
    :param html: html string
    :return: markdown string
    """
    if not html:
        raise ValueError("Cannot create markdown from empty html.")
    root_node = parse(html)
    md = Converter().convert(root_node)
    return md
