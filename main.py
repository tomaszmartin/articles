import requests
from flask import Flask, request, Response, jsonify

from app import markdown
from app.extraction import extract_body, extract_article


app = Flask(__name__)


@app.route("/api/markdown", methods=["GET"])
def get_markdown():
    url = request.args.get("url")
    response = requests.get(url)
    # TODO: Should be extended with more cases
    if response.status_code != 200:
        return Response("Not allowed!", mimetype="text/plain")
    html = response.text
    head, body = extract_article(html, url)
    md_body = markdown.from_html(body)
    md_head = markdown.from_html(head)
    if request.args.get("format") == "json":
        return jsonify({"head": md_head, "body": md_body})
    return Response(md_body, mimetype="text/plain")


if __name__ == "__main__":
    app.run(debug=True)
