import argparse
import requests

from app import markdown
from app.extraction import extract_body, extract_article


def get_markdown(url: str) -> str:
    response = requests.get(url)
    html = response.text
    head, body = extract_article(html, url)
    return markdown.from_html(body)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Utility for extracting markdown articles from web url."
    )
    parser.add_argument("--url", help="url adress that should be turned into markdown")
    args = parser.parse_args()
    if not args.url:
        print("You need to pass an url address.")
        exit()
    md = get_markdown(args.url)
    print(md)
